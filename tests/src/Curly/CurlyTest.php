<?php

/**
 * Created by PhpStorm.
 * User: carlo
 * Date: 9/20/15
 * Time: 5:51 PM
 */
use jeenorock\Curly\Curly;
use jeenorock\Curly\Response;

/**
 * Class CurlyTest
 *
 * TODO implement tests
 */
class CurlyTest extends PHPUnit_Framework_TestCase
{
    const URL = 'www.google.com';

    /**
     * @var Curly
     */
    private $curly = null;

    public function setUp()
    {
        $this->curly = new Curly();
    }


    public function testBasic()
    {
        /** @var Response */
        $r = $this->curly->request(self::URL);

        echo $r;
    }
}

<?php
/**
 * HttpMethod.php
 */

namespace jeenorock\Curly;

/**
 * HttpMethod interface.
 *
 * Contains definitions of the most common HTTP methods
 *
 * @package Curly
 *
 * @author Carlo Di Fulco <carlo.difulco@gmail.com>
 */
interface HttpMethod
{
    const GET     = 'GET';
    const PATCH   = 'PATCH';
    const POST    = 'POST';
    const PUT     = 'PUT';
    const DELETE  = 'DELETE';
    const HEAD    = 'HEAD';
    const TRACE   = 'TRACE';
    const OPTIONS = 'OPTIONS';
}

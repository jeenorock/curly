<?php
/**
 * Response.php
 */

namespace jeenorock\Curly;

/**
 * Representation of the response returned by a cURL request
 *
 * NOTE that in case of followed redirects the only response to be parsed
 * will be the one from the last redirect. To retrieve the information about the
 * previous ones you will need to access the raw response directly.
 *
 * @package Curl
 *
 * @author Carlo Di Fulco <carlo.difulco@gmail.com>
 *
 * @version 2
 */
class Response
{
    /**
     * regular expression used to find the HTTP version in a curl response
     *
     * @const string
     */
    const HTTP_VERSION_REGEXP = '/HTTP\/[1-2]\.[0-1]/';

    /**
     * HTTP status code
     *
     * @var integer
     */
    private $statusCode;

    /**
     * Returned content type header
     *
     * @var string
     */
    private $contentType;

    /**
     * Returne content length header
     *
     * @var string
     */
    private $contentLength;

    /**
     * value of the returned Location header (if any)
     *
     * @var string
     */
    private $redirectUrl;

    /**
     * Response body
     *
     * @var string or NULL when no body is returned
     */
    private $body;

    /**
     * Response headers as array
     *
     * @var array
     */
    private $headers = array();

    /**
     * Raw response returned by cURL
     *
     * @var string
     */
    private $raw = '';

    /**
     * Associative array with the info of the request
     *
     * @var array
     *
     * @see http://php.net/manual/en/function.curl-getinfo.php
     */
    private $info = array();

    /**
     * Initializes the response object.
     *
     * @param string $rawResponse
     * @param array  $info associative array with the info of the request
     */
    public function __construct($rawResponse, array $info)
    {
        $this->raw  = $rawResponse;
        $this->info = $info;

        $this->parseRaw($rawResponse);
    }

    /**
     * Returns the HTTP status code
     *
     * @return integer
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Returns headers as array
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Returns the body as string
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Returns all the headers with a specified key
     *
     * @param string $key
     *
     * @return array
     */
    public function getHeadersByKey($key)
    {
        $matches = preg_grep("/$key:.*/", $this->headers);

        return $matches;
    }

    /**
     * returns the raw response returned by cURL
     *
     * @return string
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * Returns an associative array with the info of the request
     *
     * @see http://php.net/manual/en/function.curl-getinfo.php
     *
     * @return array
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Returns the content type found in the headers
     *
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * Returns the content length found in the headers
     *
     * @return integer
     */
    public function getContentLength()
    {
        return $this->contentLength;
    }

    /**
     * Returns the value of the Location header, i.e. the redirect URL
     *
     * @return mixed string when set, NULL otherwise
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->getBody();
    }

    /**
     * Parses the raw response and initializes the data structure.
     * The logic to parse follows the rules specified in RFC2616:
     *
     * Status-Line
     * (( general-header | response-header | entity-header ) CRLF)*
     * CRLF
     * [ message-body ]
     *
     * @see http://www.w3.org/Protocols/rfc2616/rfc2616-sec6.html
     *
     * @param string $raw
     */
    private function parseRaw($raw)
    {
        // split the raw responses using the http version in the status line as separator
        // that allows to find the beginning of every response
        $responses = preg_split(self::HTTP_VERSION_REGEXP, $raw);

        // retrieve the last response (in case of redirects)
        $last = end($responses);
        // and separate the headers from the body (double CRLF)
        $last = explode("\r\n\r\n", $last);

        // split the headers (on CRLF) and extract the status code...
        $headers          = explode("\r\n", $last[0]);
        $this->statusCode = (integer)trim($headers[0]);

        unset($headers[0]);

        // set the headers and the body (if any)
        $this->headers = array_values($headers);
        $this->body    = (isset($last[1])) ? $last[1] : NULL;

        // Empty string is not an allowed body so we NULL it.
        if(TRUE === empty($this->body)){
            $this->body = NULL;
        }

        // extract content type...
        $contentType       = $this->getHeadersByKey('Content-Type');
        $contentType       = explode(':', current($contentType));
        $this->contentType = isset($contentType[1]) ? trim($contentType[1]) : NULL;

        // ...content length...
        $contentLength       = $this->getHeadersByKey('Content-Length');
        $contentLength       = explode(':', current($contentLength));
        $this->contentLength = isset($contentLength[1]) ? (integer)$contentLength[1] : 0;

        // ...and redirectUrl
        $location          = $this->getHeadersByKey('Location');
        $location          = current($location) !== FALSE ? current($location) : '';
        $location          = str_replace('Location: ', '', $location);
        $this->redirectUrl = !empty($location) ? $location : NULL;
    }
}

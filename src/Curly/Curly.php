<?php
/**
 * Curly.php
 */

namespace jeenorock\Curly;

use Psr\Log\LoggerAwareInterface;
use \Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use \RuntimeException;

/**
 * Curly is a simple wrapper around cURL.
 *
 * @package Curly
 *
 * @author Carlo Di Fulco <carlo.difulco@iconicfuture.com>
 *
 * @version 1.0
 */
class Curly implements LoggerAwareInterface
{
    const VERSION = '1.0';

    /**
     * User agent string
     *
     * @var string
     */
    private $userAgent;

    /**
     * Request timeout in seconds.
     * Default to 5s.
     *
     * @var integer
     */
    private $timeout = 5;

    /**
     * Array containing the name/value header pairs
     *
     * @var array
     */
    private $headers = array();

    /**
     * Array containing the name/value option pairs
     *
     * @var array
     */
    private $options = array();

    /**
     * Logger object
     *
     * @var Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * When $withUserAgent is set to TRUE the constructor
     * adds a user agent to the calls
     *
     * @param Psr\Log\LoggerInterface  $logger
     * @param string $appName the name of the application performing the cURL request
     * @param string $appVersion the version of the app performing the cURL request
     */
    public function __construct(LoggerInterface $logger = null, $appName = 'UNKNOWN', $appVersion = 'UNKNOWN')
    {
        if($logger === null) {
            $logger = new NullLogger();
        }

        $this->setLogger($logger);
        $curlVersion  = curl_version();

        $userAgent  = $appName . '/' . $appVersion . ', ' . __CLASS__ . '/'. self::VERSION .', ';
        $userAgent .= 'cURL/' . $curlVersion['version'] . ' on ' . $curlVersion['host'] .', ';
        $userAgent .= 'PHP/' . PHP_VERSION . ' OS/' . PHP_OS . ', ';

        $this->setUserAgent($userAgent);
    }

    /**
     * Sets the user agent to send during the cURL requests
     *
     * @param string $userAgent
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
    }

    /**
     * Adds a header
     *
     * @param string $header
     */
    public function addHeader($header)
    {
        $this->headers[] = $header;
    }

    /**
     * Flushes the headers array
     */
    public function flushHeaders()
    {
        $this->headers = array();
    }

    /**
     * Adds a options name/value pair
     *
     * @param string $name
     * @param string $value
     */
    public function addOption($name, $value)
    {
        $this->options[$name] = $value;
    }

    /**
     * Flushes the options array
     */
    public function flushOptions()
    {
        $this->options = array();
    }

    /**
     * Set the timeout (in seconds) for the cURL request.
     *
     * @param integer $seconds
     */
    public function setTimeout($seconds)
    {
        $this->timeout = $seconds;
    }

    /**
     * Activates debugging for the cURL request
     */
    public function setDebug()
    {
        $this->addOption(CURLOPT_VERBOSE, TRUE);
    }

    /**
     * Performs a cURL request to the specified url with the specified method.
     *
     * @param string $url
     * @param string $method default GET
     * @param array  $data
     *
     * @return \Iconicfuture\Curl\Response
     *
     * @return RuntimeException
     */
    public function request($url, $method = HttpMethod::GET, array $data = array())
    {
        // initialize
        $handle = curl_init($url);

        if(!is_resource($handle)){
            throw new RuntimeException('cURL failed during initialization!');
        }

        if(!empty($data)) {
            $queryString       = http_build_query($data, '', '&');
            $queryStringLength = strlen($queryString);

            $this->addHeader('Content-Length: ' . $queryStringLength);

            // curl options to send a request body
            $this->addOption(CURLOPT_POSTFIELDS, $queryString);
            $this->addOption(CURLOPT_POST, $queryStringLength);
        }

        // prepare everything for the request
        $this->prepare($handle, $method);

        // perform request
        $responseString = curl_exec($handle);

        // error handling
        if($responseString === FALSE){
            $errorMessage = 'cURL ErrorCode('.curl_errno($handle).') - ' . curl_error($handle);
            $this->logger->error($errorMessage);
            throw new RuntimeException('cURL failure: ' . $errorMessage);
        }

        $info = curl_getinfo($handle);

        curl_close($handle);

        return new Response($responseString, $info);
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    /**
     * Prepares handle for the call
     *
     * @param resource $handle
     * @param string $method
     */
    private function prepare($handle, $method)
    {
        // standard settings
        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($handle, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($handle, CURLOPT_HEADER, TRUE);

        // force no body in case of head request
        // http://php.net/manual/en/function.curl-setopt.php
        if($method === HttpMethod::HEAD){
            curl_setopt($handle, CURLOPT_NOBODY, TRUE);
        }

        if(!empty($this->headers)){
            curl_setopt($handle, CURLOPT_HTTPHEADER, $this->headers);
        }

        // set cURL options
        foreach($this->options as $name => $value){
            if(!curl_setopt($handle, $name, $value)){
                curl_close($handle);
                throw new RuntimeException('Setting of cURL option "'. $name . '" failed!');
            }
        }
    }
}
